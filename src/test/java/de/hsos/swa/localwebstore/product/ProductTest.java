package de.hsos.swa.localwebstore.product;

import de.hsos.swa.localwebstore.product.boundary.ProductResource;
import de.hsos.swa.localwebstore.util.BaseTest;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.QuarkusTestProfile;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

import static io.restassured.RestAssured.given;

/**
 * This Class defines Unit Tests for Products
 * @author Kevin Lucas Simon
 */
@QuarkusTest
@TestProfile(ProductTest.ProductTestProfile.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestHTTPEndpoint(ProductResource.class)
public class ProductTest extends BaseTest {
    public static class ProductTestProfile implements QuarkusTestProfile {}

    @Test
    @Order(0)
    @TestTransaction
    public void testInit() {
        initTestEnvironment();
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testList() {
        given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .when()
                .get()
                .then().statusCode(200);

        String body = given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(2, resultList.size());
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testListWithoutStore() {
        given()
                .auth().oauth2(token)
                .when()
                .get()
                .then().statusCode(200);

        String body = given()
                .auth().oauth2(token)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(0, resultList.size());
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testListWithTagFilter() {
        Collection<Integer> tags = new ArrayList<>();
        tags.add(4);
        given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .param("filter[tag-id]", tags)
                .when()
                .get()
                .then().statusCode(200);

        String body = given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .param("filter[tag-id]", tags)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(1, resultList.size());
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieve() {
        given()
                .auth().oauth2(token)
                .when()
                .get("6")
                .then().statusCode(200);
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieveWithWrongId() {
        given()
                .auth().oauth2(token)
                .when()
                .get("111")
                .then().statusCode(404);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreate() {
        JsonObject productToCreate = Json.createObjectBuilder()
                .add("name", "Keksdose")
                .add("description", "Hier kannst du deine Kekse gut aufbewahren")
                .add("price", 12)
                .add("visible", true)
                .add("store", Json.createObjectBuilder()
                        .add("id", 2)
                        .build())
                .add("tags", Json.createArrayBuilder().build())
                .build();

        given().body(productToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(200);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateMissingAttributes() {
        JsonObject productToCreate = Json.createObjectBuilder()
                .add("name", "Keksdose")
                .add("store", Json.createObjectBuilder()
                        .add("id", 2)
                        .build())
                .add("tags", Json.createArrayBuilder().build())
                .build();

        given().body(productToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithStoreNotOwnedInvalid() {
        JsonObject productToCreate = Json.createObjectBuilder()
                .add("name", "Keksdose")
                .add("description", "Hier kannst du deine Kekse gut aufbewahren")
                .add("price", 12)
                .add("visible", true)
                .add("store", Json.createObjectBuilder()
                        .add("id", 1)
                        .build())
                .add("tags", Json.createArrayBuilder().build())
                .build();

        given().body(productToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithTag() {
        JsonObject productToCreate = Json.createObjectBuilder()
                .add("name", "Keksdose")
                .add("description", "Hier kannst du deine Kekse gut aufbewahren")
                .add("price", 12)
                .add("visible", true)
                .add("store", Json.createObjectBuilder()
                        .add("id", 2)
                        .build())
                .add("tags", Json.createArrayBuilder()
                        .add(Json.createObjectBuilder().add("id", 3).build())
                        .build())
                .build();

        given().body(productToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(200);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithNegativePriceInvalid() {
        JsonObject productToCreate = Json.createObjectBuilder()
                .add("name", "Keksdose")
                .add("description", "Hier kannst du deine Kekse gut aufbewahren")
                .add("price", -12)
                .add("visible", true)
                .add("store", Json.createObjectBuilder()
                        .add("id", 2)
                        .build())
                .add("tags", Json.createArrayBuilder().build())
                .build();

        given().body(productToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdate() {
        JsonObject productToUpdate = Json.createObjectBuilder()
                .add("name", "GEHACKTES PRODUKT")
                .add("description", "DIESE PRODUKTSEITE WURDE GEHACKT, MUHAHAHA")
                .add("price", 12)
                .add("visible", true)
                .add("tags", Json.createArrayBuilder().build())
                .build();

        given().body(productToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("6")
                .then().statusCode(200);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdateTagsInvalid() {
        JsonObject productToUpdate = Json.createObjectBuilder()
                .add("name", "GEHACKTES PRODUKT")
                .add("description", "DIESE PRODUKTSEITE WURDE GEHACKT, MUHAHAHA")
                .add("price", 12)
                .add("visible", true)
                .add("tags", Json.createArrayBuilder()
                        .add(Json.createObjectBuilder().add("id", 111).build())
                        .build())
                .build();

        given().body(productToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("6")
                .then().statusCode(400);
    }

    @Test
    @Order(5)
    @TestTransaction
    public void testDelete() {
        given()
                .auth().oauth2(token)
                .when()
                .delete("6")
                .then().statusCode(200);
    }
}
