package de.hsos.swa.localwebstore.product;

import de.hsos.swa.localwebstore.product.boundary.TagResource;
import de.hsos.swa.localwebstore.util.BaseTest;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.QuarkusTestProfile;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

import java.io.StringReader;

import static io.restassured.RestAssured.given;

/**
 * This Class defines Unit Tests for Tags
 * @author Kevin Lucas Simon
 */
@QuarkusTest()
@TestProfile(TagTest.TagTestProfile.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestHTTPEndpoint(TagResource.class)
public class TagTest extends BaseTest {
    public static class TagTestProfile implements QuarkusTestProfile {}

    @Test
    @Order(0)
    @TestTransaction
    public void testInit() {
        initTestEnvironment();
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testList() {
        given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .when()
                .get()
                .then().statusCode(200);

        String body = given()
                .auth().oauth2(token)
                .param("filter[store-id]", 2)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(3, resultList.size());
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testListWrongStoreInvalid() {
        given()
                .auth().oauth2(token)
                .param("filter[store-id]", 111)
                .when()
                .get()
                .then().statusCode(200);

        String body = given()
                .auth().oauth2(token)
                .param("filter[store-id]", 111)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(0, resultList.size());
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testListNoStoreInvalid() {
        given()
                .auth().oauth2(token)
                .when()
                .get()
                .then().statusCode(400);
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieve() {
        given()
                .auth().oauth2(token)
                .when()
                .get("3")
                .then().statusCode(200);
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieveWithWrongIdInvalid() {
        given()
                .auth().oauth2(token)
                .when()
                .get("111")
                .then().statusCode(404);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreate() {
        JsonObject tagToCreate = Json.createObjectBuilder()
                .add("name", "Geschenkideen")
                .add("store", Json.createObjectBuilder()
                        .add("id", 2)
                        .build())
                .build();

        given().body(tagToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(200);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithoutStoreInvalid() {
        JsonObject tagToCreate = Json.createObjectBuilder()
                .add("name", "Geschenkideen")
                .build();

        given().body(tagToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithWrongStoreInvalid() {
        JsonObject tagToCreate = Json.createObjectBuilder()
                .add("name", "Geschenkideen")
                .add("store", Json.createObjectBuilder()
                        .add("id", 111)
                        .build())
                .build();

        given().body(tagToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdate() {
        JsonObject tagToUpdate = Json.createObjectBuilder()
                .add("name", "Neuigkeiten")
                .build();

        given().body(tagToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("5")
                .then().statusCode(200);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdateWrongIdInvalid() {
        JsonObject tagToUpdate = Json.createObjectBuilder()
                .add("name", "Neuigkeiten")
                .build();

        given().body(tagToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("111")
                .then().statusCode(400);
    }

    @Test
    @Order(5)
    @TestTransaction
    public void testDelete() {
        given()
                .auth().oauth2(token)
                .when()
                .delete("5")
                .then().statusCode(200);
    }
}
