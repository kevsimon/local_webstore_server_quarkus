package de.hsos.swa.localwebstore.util;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.keycloak.representations.AccessTokenResponse;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

import static io.restassured.RestAssured.given;

/**
 * This Class defines the base of Unit Test classes
 * @author Kevin Lucas Simon
 */
public class BaseTest {
    private final String username = "manni@hs-osnabrueck.de";
    private final String password = "shopping";

    private final String apiURL = "http://localhost:8080/api/v1/";
    private final String authURL = "http://localhost:8180/auth/realms/quarkus/protocol/openid-connect/token";

    protected String token;

    // auto login and initialisation to refresh token
    @BeforeEach
    protected void setup() {
        token = getAuthToken();
    }

    // grüße gehen raus an https://github.com/quarkusio/quarkus/blob/dc4ea361754252d8604d2a4945982f7fbba75728/extensions/keycloak-authorization/deployment/src/test/java/io/quarkus/keycloak/pep/test/PolicyEnforcerTest.java#L135
    protected String getAuthToken() {
        return RestAssured
                .given()
                .param("grant_type", "password")
                .param("username", username)
                .param("password", password)
                .param("client_id", "backend-service")
                .param("client_secret", "secret")
                .when()
                .post(authURL)
                .as(AccessTokenResponse.class).getToken();
    }

    protected void initTestEnvironment() {
        // create store with id 2
        JsonObject address1 = Json.createObjectBuilder()
                .add("city", "Osnabrück")
                .add("postcode", "49080")
                .add("street", "Jahnstraße")
                .add("streetNumber", "17a")
                .build();
        JsonObject store2 = Json.createObjectBuilder()
                .add("name", "WG Laden")
                .add("description", "Alles was WG so braucht")
                .add("phoneNumber", "110")
                .add("visible", true)
                .add("address", address1)
                .build();
        given().body(store2.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/stores")
                .then().statusCode(200);


        // create tag with id 3
        JsonObject storeId2 = Json.createObjectBuilder()
                .add("id", 2)
                .build();
        JsonObject tag3 = Json.createObjectBuilder()
                .add("name", "Lesenswertes")
                .add("store", storeId2)
                .build();
        given().body(tag3.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/tags")
                .then().statusCode(200);


        // create tag with id 4
        JsonObject tag4 = Json.createObjectBuilder()
                .add("name", "Schmackhaftes")
                .add("store", storeId2)
                .build();
        given().body(tag4.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/tags")
                .then().statusCode(200);


        // create tag with id 5
        JsonObject tag5 = Json.createObjectBuilder()
                .add("name", "Ekliges")
                .add("store", storeId2)
                .build();
        given().body(tag5.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/tags")
                .then().statusCode(200);


        // create product with id 6
        JsonArray tagsProduct6 = Json.createArrayBuilder().add(
                Json.createObjectBuilder()
                        .add("id", 3)
                        .build()
        ).build();
        JsonObject product6 = Json.createObjectBuilder()
                .add("name", "Harry Potter")
                .add("description", "Erfahre die magische Geschichte von Harry Potter!")
                .add("price", 12)
                .add("visible", true)
                .add("store", storeId2)
                .add("tags", tagsProduct6)
                .build();
        given().body(product6.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/products")
                .then().statusCode(200);


        // create product with id 7
        JsonArray tagsProduct7 = Json.createArrayBuilder().add(
                Json.createObjectBuilder()
                        .add("id", 3)
                        .build()
        ).add(
                Json.createObjectBuilder()
                        .add("id", 4)
                        .build()
        ).build();
        JsonObject product7 = Json.createObjectBuilder()
                .add("name", "WG Kochbuch")
                .add("description", "Nie wieder was ekliges essen!")
                .add("price", 12)
                .add("visible", true)
                .add("store", storeId2)
                .add("tags", tagsProduct7)
                .build();
        given().body(product7.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post(apiURL + "vendor/products")
                .then().statusCode(200);
    }
}
