package de.hsos.swa.localwebstore.store;

import de.hsos.swa.localwebstore.store.boundary.StoreResource;
import de.hsos.swa.localwebstore.util.BaseTest;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.QuarkusTestProfile;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

import java.io.StringReader;

import static io.restassured.RestAssured.given;

/**
 * This Class defines Unit Tests for Stores
 * @author Kevin Lucas Simon
 */
@QuarkusTest
@TestProfile(StoreTest.StoreTestProfile.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestHTTPEndpoint(StoreResource.class)
public class StoreTest extends BaseTest {
    public static class StoreTestProfile implements QuarkusTestProfile {}

    @Test
    @Order(0)
    @TestTransaction
    public void testInit() {
        initTestEnvironment();
    }

    @Test
    @Order(1)
    @TestTransaction
    public void testList() {
        // http response
        given()
                .auth().oauth2(token)
                .when()
                .get()
                .then().statusCode(200);

        // content length
        String body = given()
                .auth().oauth2(token)
                .when()
                .get()
                .getBody().asString();
        JsonArray resultList = Json.createReader(new StringReader(body)).readArray();
        Assertions.assertEquals(1, resultList.size());
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieve() {
        given()
                .auth().oauth2(token)
                .when()
                .get("2")
                .then().statusCode(200);
    }

    @Test
    @Order(2)
    @TestTransaction
    public void testRetrieveWithWrongIdInvalid() {
        given()
                .auth().oauth2(token)
                .when()
                .get("111")
                .then().statusCode(404);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreate() {
        JsonObject addressToCreate = Json.createObjectBuilder()
                .add("city", "Osnabrück")
                .add("postcode", "49080")
                .add("street", "Jahnstraße")
                .add("streetNumber", "1")
                .build();

        JsonObject storeToCreate = Json.createObjectBuilder()
                .add("name", "Geschäft")
                .add("description", "Hier kannst du alles kaufen")
                .add("phoneNumber", "110")
                .add("visible", true)
                .add("address", addressToCreate)
                .build();

        given().body(storeToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(200);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateMissingAttributesInvalid() {
        JsonObject addressToCreate = Json.createObjectBuilder()
                .add("city", "Osnabrück")
                .add("postcode", "49080")
                .add("streetNumber", "1")
                .build();

        JsonObject storeToCreate = Json.createObjectBuilder()
                .add("name", "Geschäft")
                .add("address", addressToCreate)
                .build();

        given().body(storeToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(3)
    @TestTransaction
    public void testCreateWithEmptyAddressInvalid() {
        JsonObject storeToCreate = Json.createObjectBuilder()
                .add("name", "Geschäft")
                .add("description", "Hier kannst du alles kaufen")
                .add("phoneNumber", "110")
                .add("visible", true)
                .build();

        given().body(storeToCreate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .post()
                .then().statusCode(400);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdate() {
        JsonObject addressToUpdate = Json.createObjectBuilder()
                .add("city", "Belm")
                .add("postcode", "49191")
                .add("street", "Ringstraße")
                .add("streetNumber", "27")
                .build();

        JsonObject storeToUpdate = Json.createObjectBuilder()
                .add("name", "Edeka")
                .add("description", "Essen gibt es reichlich")
                .add("phoneNumber", "112")
                .add("visible", false)
                .add("address", addressToUpdate)
                .build();

        given().body(storeToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("2")
                .then().statusCode(200);
    }

    @Test
    @Order(4)
    @TestTransaction
    public void testUpdateWithoutAddressInvalid() {
        JsonObject storeToUpdate = Json.createObjectBuilder()
                .add("name", "Edeka")
                .add("description", "Essen gibt es reichlich")
                .add("phoneNumber", "112")
                .add("visible", false)
                .build();

        given().body(storeToUpdate.toString())
                .auth().oauth2(token)
                .contentType(ContentType.JSON)
                .patch("2")
                .then().statusCode(400);
    }

    @Test
    @Order(5)
    @TestTransaction
    public void testDelete() {
        given()
                .auth().oauth2(token)
                .when()
                .delete("2")
                .then().statusCode(200);
    }
}
