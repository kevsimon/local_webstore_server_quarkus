package de.hsos.swa.localwebstore.store.boundary;

import de.hsos.swa.localwebstore.store.entity.Address;

/**
 * This data transfer object is used for an representation of the Address Entity for other DTOs
 * @author Kevin Lucas Simon
 */
public class AddressDTO {
    private String street;
    private String streetNumber;
    private String postcode;
    private String city;

    public AddressDTO() {}

    public AddressDTO(Address address) {
        this.setStreet(address.getStreet());
        this.setStreetNumber(address.getStreetNumber());
        this.setPostcode(address.getPostcode());
        this.setCity(address.getCity());
    }

    public Address export() {
        Address address = new Address();

        address.setStreet(this.getStreet());
        address.setStreetNumber(this.getStreetNumber());
        address.setPostcode(this.getPostcode());
        address.setCity(this.getCity());

        return address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
