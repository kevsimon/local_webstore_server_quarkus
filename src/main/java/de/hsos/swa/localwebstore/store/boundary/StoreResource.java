package de.hsos.swa.localwebstore.store.boundary;

import de.hsos.swa.localwebstore.store.control.StoreEditorService;
import de.hsos.swa.localwebstore.store.control.StoreReaderService;
import de.hsos.swa.localwebstore.store.entity.Store;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This class accepts requests and gives results for the Store-Endpoint
 * @author Kevin Lucas Simon
 */
@Path("/api/v1/vendor/stores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "SecurityScheme", type = SecuritySchemeType.HTTP, scheme = "Bearer")
public class StoreResource {
    @Inject
    SecurityIdentity identity;

    @Inject
    StoreReaderService storeReaderService;
    @Inject
    StoreEditorService storeEditorService;

    @GET
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Collection<StoreForListDTO> getStoreList(
            @QueryParam("page[number]") int pageNumber, @QueryParam("page[size]") int pageSize) {
        // Convert Store Collection to StoreDTO Collection and return result
        Collection<Store> stores = storeReaderService.getStoreList(getAuthenticatedUserId(), pageNumber, pageSize);
        Collection<StoreForListDTO> storeDTOs = new ArrayList<>();
        stores.forEach(store -> storeDTOs.add(new StoreForListDTO(store)));
        return storeDTOs;
    }

    @POST
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public StoreForDetailDTO createStore(StoreForCreationDTO newStore) {
        return new StoreForDetailDTO(storeEditorService.createStore(getAuthenticatedUserId(), newStore.export()));
    }

    @GET
    @Path("/{store-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public StoreForDetailDTO getStoreDetail(@PathParam("store-id") long storeId) {
        return new StoreForDetailDTO(storeReaderService.getStoreDetail(getAuthenticatedUserId(), storeId));
    }

    @PATCH
    @Path("/{store-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public StoreForDetailDTO updateStore(@PathParam("store-id") long storeId, StoreForCreationDTO updatedStore) {
        Store store = updatedStore.export();
        store.setId(storeId);
        return new StoreForDetailDTO(storeEditorService.updateStore(getAuthenticatedUserId(), store));
    }

    @DELETE
    @Path("/{store-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Response deleteStore(@PathParam("store-id") long storeId) {
        storeEditorService.deleteStore(getAuthenticatedUserId(), storeId);
        return Response.ok().build();
    }

    private String getAuthenticatedUserId() {
        return ((DefaultJWTCallerPrincipal) identity.getPrincipal()).getSubject();
    }
}
