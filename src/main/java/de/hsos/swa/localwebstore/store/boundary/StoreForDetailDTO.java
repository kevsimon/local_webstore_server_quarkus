package de.hsos.swa.localwebstore.store.boundary;

import de.hsos.swa.localwebstore.store.entity.Store;

/**
 * This data transfer object is used for the detail view of Store
 * @author Kevin Lucas Simon
 */
public class StoreForDetailDTO {
    private long id;
    private String name;
    private String description;
    private String phoneNumber;
    private boolean visible;
    private AddressDTO address;

    public StoreForDetailDTO() {}

    public StoreForDetailDTO(Store store) {
        this.setId(store.getId());
        this.setName(store.getName());
        this.setDescription(store.getDescription());
        this.setPhoneNumber(store.getPhoneNumber());
        this.setVisible(store.isVisible());
        this.setAddress(new AddressDTO(store.getAddress()));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
