package de.hsos.swa.localwebstore.store.boundary;

import de.hsos.swa.localwebstore.store.entity.Store;

/**
 * This data transfer object is used for the creation of Store
 * @author Kevin Lucas Simon
 */
public class StoreForCreationDTO {
    private String name;
    private String description;
    private String phoneNumber;
    private boolean visible;
    private AddressDTO address;

    public StoreForCreationDTO() {}

    public StoreForCreationDTO(Store store) {
        this.setName(store.getName());
        this.setDescription(store.getDescription());
        this.setPhoneNumber(store.getPhoneNumber());
        this.setVisible(store.isVisible());
        this.setAddress(new AddressDTO(store.getAddress()));
    }

    public Store export() {
        Store store = new Store();

        store.setName(this.getName());
        store.setDescription(this.getDescription());
        store.setPhoneNumber(this.getPhoneNumber());
        store.setVisible(this.isVisible());

        if(this.address != null)
            store.setAddress(this.address.export());

        return store;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
