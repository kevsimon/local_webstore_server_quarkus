package de.hsos.swa.localwebstore.store.boundary;

import de.hsos.swa.localwebstore.store.entity.Store;

/**
 * This data transfer object is used for the list view of Store
 * @author Kevin Lucas Simon
 */
public class StoreForListDTO {
    private long id;
    private String name;

    public StoreForListDTO() {}

    public StoreForListDTO(Store store) {
        this.setId(store.getId());
        this.setName(store.getName());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
