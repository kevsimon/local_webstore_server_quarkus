package de.hsos.swa.localwebstore.store.gateway;

import de.hsos.swa.localwebstore.store.control.StoreEditorService;
import de.hsos.swa.localwebstore.store.control.StoreReaderService;
import de.hsos.swa.localwebstore.store.entity.Address;
import de.hsos.swa.localwebstore.store.entity.Store;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * This Implementation manages the Stores in the Database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class StoreRepository implements StoreReaderService, StoreEditorService {
    @Inject
    StorePanacheRepository storeRepository;
    @Inject
    AddressPanacheRepository addressRepository;

    @Override
    public Collection<Store> getStoreList(String userId, int pageNumber, int pageSize) {
        // get all user related stores
        Stream<Store> storeStream = storeRepository.streamAll()
                .filter(store -> store.getOwnerId().equals(userId));

        // paging
        if(pageNumber != 0 || pageSize != 0) {
            if(pageNumber <= 0) pageNumber = 1;
            if(pageSize <= 0) pageSize = 10;
            storeStream = storeStream
                    .skip(pageSize * (pageNumber - 1))
                    .limit(pageSize);
        }

        // return results
        return storeStream.collect(Collectors.toList());
    }

    @Override
    public Store getStoreDetail(String userId, long storeId) {
        Store store = storeRepository.findById(storeId);
        // check if user owns the store
        if(store == null || !store.getOwnerId().equals(userId))
            throw new NotFoundException();
        return store;
    }

    @Override
    @Transactional
    public Store createStore(String userId, Store store) {
        try {
            // fill remaining fields
            store.setOwnerId(userId);

            // persist data
            addressRepository.persist(store.getAddress());
            storeRepository.persist(store);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return store;
    }

    @Override
    @Transactional
    public Store updateStore(String userId, Store store) {
        try {
            // fill remaining fields in address and store
            Address address = store.getAddress();
            address.setId(getStoreDetail(userId, store.getId()).getAddress().getId());
            store.setOwnerId(userId);
            store.setAddress(address);
            Store foundStore = storeRepository.findById(store.getId());
            store.setProducts(foundStore.getProducts());
            store.setTags(foundStore.getTags());

            // persist data
            addressRepository.getEntityManager().merge(address);
            storeRepository.getEntityManager().merge(store);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return store;
    }

    @Override
    @Transactional
    public void deleteStore(String userId, long storeId) {
        storeRepository.delete(getStoreDetail(userId, storeId));
    }
}
