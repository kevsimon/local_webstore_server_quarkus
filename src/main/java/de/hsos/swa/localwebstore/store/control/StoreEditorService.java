package de.hsos.swa.localwebstore.store.control;

import de.hsos.swa.localwebstore.store.entity.Store;

/**
 * This Interface is for editing Stores
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface StoreEditorService {
    Store createStore(String userId, Store store);
    Store updateStore(String userId, Store store);
    void deleteStore(String userId, long storeId);
}
