package de.hsos.swa.localwebstore.store.gateway;

import de.hsos.swa.localwebstore.store.entity.Store;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This Repository is the Interface for the Stores in the database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class StorePanacheRepository implements PanacheRepository<Store> {}
