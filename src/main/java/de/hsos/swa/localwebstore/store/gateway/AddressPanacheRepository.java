package de.hsos.swa.localwebstore.store.gateway;

import de.hsos.swa.localwebstore.store.entity.Address;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This Repository is the Interface for the Addresses in the database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class AddressPanacheRepository implements PanacheRepository<Address> {
}
