package de.hsos.swa.localwebstore.store.control;

import de.hsos.swa.localwebstore.store.entity.Store;

import java.util.Collection;

/**
 * This Interface is for reading Stores
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface StoreReaderService {
    Collection<Store> getStoreList(String userId, int pageNumber, int pageSize);
    Store getStoreDetail(String userId, long storeId);
}
