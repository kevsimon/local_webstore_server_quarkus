package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This data transfer object is used for the creation of Tags
 * @author Kevin Lucas Simon
 */
public class TagForCreationDTO {
    private String name;
    private StoreReducedDTO store;

    public TagForCreationDTO() {}

    public TagForCreationDTO(Tag tag) {
        this.setName(tag.getName());
        this.setStore(new StoreReducedDTO(tag.getStore()));
    }

    public Tag export() {
        Tag tag = new Tag();

        tag.setName(this.getName());

        if(this.store != null)
        tag.setStore(this.store.export());

        return tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreReducedDTO getStore() {
        return store;
    }

    public void setStore(StoreReducedDTO store) {
        this.store = store;
    }
}
