package de.hsos.swa.localwebstore.product.gateway;

import de.hsos.swa.localwebstore.product.entity.Tag;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This Repository is the Interface for the Tags in the database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class TagPanacheRepository implements PanacheRepository<Tag> {
}
