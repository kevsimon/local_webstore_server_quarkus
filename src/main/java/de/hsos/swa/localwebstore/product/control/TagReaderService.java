package de.hsos.swa.localwebstore.product.control;

import de.hsos.swa.localwebstore.product.entity.Tag;

import java.util.Collection;

/**
 * This Interface is for reading Tags
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface TagReaderService {
    Collection<Tag> getTagListFromStore(String userId, int pageNumber, int pageSize, Long storeId);
    Tag getTagDetail(String userId, long tagId);
}
