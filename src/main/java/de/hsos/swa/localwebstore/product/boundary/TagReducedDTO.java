package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This data transfer object is used for an reduced representation of the Tag Entity for other DTOs
 * @author Kevin Lucas Simon
 */
public class TagReducedDTO {
    private long id;

    public TagReducedDTO() {}

    public TagReducedDTO(Tag tag) {
        this.setId(tag.getId());
    }

    public Tag export() {
        Tag tag = new Tag();

        tag.setId(this.getId());

        return tag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
