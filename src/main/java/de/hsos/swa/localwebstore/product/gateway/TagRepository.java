package de.hsos.swa.localwebstore.product.gateway;

import de.hsos.swa.localwebstore.product.control.TagEditorService;
import de.hsos.swa.localwebstore.product.control.TagReaderService;
import de.hsos.swa.localwebstore.product.entity.Product;
import de.hsos.swa.localwebstore.product.entity.Tag;
import de.hsos.swa.localwebstore.store.entity.Store;
import de.hsos.swa.localwebstore.store.gateway.StorePanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This Implementation manages the Tags in the Database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class TagRepository implements TagReaderService, TagEditorService {
    @Inject
    TagPanacheRepository tagPanacheRepository;
    @Inject
    StorePanacheRepository storePanacheRepository;

    @Override
    public Collection<Tag> getTagListFromStore(String userId, int pageNumber, int pageSize, Long storeId) {
        // validate parameter
        if(storeId == null)
            throw new BadRequestException();

        // get all store and user related tags
        Stream<Tag> tagStream = tagPanacheRepository.streamAll()
                .filter(tag -> tag.getOwnerId().equals(userId))
                .filter(tag -> tag.getStore().getId() == storeId);

        // paging
        if(pageNumber != 0 || pageSize != 0) {
            if(pageNumber <= 0) pageNumber = 1;
            if(pageSize <= 0) pageSize = 10;
            tagStream = tagStream
                    .skip(pageSize * (pageNumber - 1))
                    .limit(pageSize);
        }

        // return result list
        return tagStream.collect(Collectors.toList());
    }

    @Override
    public Tag getTagDetail(String userId, long tagId) {
        Tag tag = tagPanacheRepository.findById(tagId);
        // validate existence and ownership
        if(tag == null || !tag.getOwnerId().equals(userId))
            throw new NotFoundException();
        return tag;
    }

    @Override
    @Transactional
    public Tag createTag(String userId, Tag tag) {
        try {
            // validate store existence and ownership
            Store store = storePanacheRepository.findById(tag.getStore().getId());
            if(store == null || !store.getOwnerId().equals(userId))
                throw new BadRequestException();
            tag.setStore(store);
            tag.setOwnerId(userId);

            // persist data
            tagPanacheRepository.persist(tag);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return tag;
    }

    @Override
    @Transactional
    public Tag updateTag(String userId, Tag tag) {
        try {
            // validate ownership
            Tag foundTag = tagPanacheRepository.findById(tag.getId());
            if(foundTag == null || !foundTag.getOwnerId().equals(userId))
                throw new BadRequestException();
            tag.setOwnerId(userId);
            tag.setStore(foundTag.getStore());

            // persist data
            tagPanacheRepository.getEntityManager().merge(tag);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return tag;
    }

    @Override
    @Transactional
    public void deleteTag(String userId, long tagId) {
        tagPanacheRepository.delete(getTagDetail(userId, tagId));
    }
}
