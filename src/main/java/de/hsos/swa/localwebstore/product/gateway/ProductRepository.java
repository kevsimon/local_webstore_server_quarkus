package de.hsos.swa.localwebstore.product.gateway;

import de.hsos.swa.localwebstore.product.control.ProductEditorService;
import de.hsos.swa.localwebstore.product.control.ProductReaderService;
import de.hsos.swa.localwebstore.product.entity.Product;
import de.hsos.swa.localwebstore.product.entity.Tag;
import de.hsos.swa.localwebstore.store.entity.Store;
import de.hsos.swa.localwebstore.store.gateway.StorePanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This Implementation manages the Products in the Database
 * It will be used for injection
 * @author Kevin Lucas Simon
 */
@ApplicationScoped
public class ProductRepository implements ProductReaderService, ProductEditorService {
    @Inject
    ProductPanacheRepository productPanacheRepository;
    @Inject
    TagPanacheRepository tagPanacheRepository;
    @Inject
    StorePanacheRepository storePanacheRepository;

    @Override
    public Collection<Product> getProductList(String userId, int pageNumber, int pageSize,
                                              long storeId, Collection<Long> tagIds) {
        // basic Stream with user and store filtering
        Stream<Product> productStream = productPanacheRepository.streamAll()
                .filter(product -> product.getOwnerId().equals(userId))
                .filter(product -> product.getStore().getId() == storeId);

        // tag filtering
        if(tagIds != null) {
            for (Long tagId : tagIds)
                productStream = productStream.filter(product ->
                        product.getTags().contains(tagPanacheRepository.findById(tagId)));
        }

        // paging
        if(pageNumber != 0 || pageSize != 0) {
            if(pageNumber <= 0) pageNumber = 1;
            if(pageSize <= 0) pageSize = 10;
            productStream = productStream
                    .skip(pageSize * (pageNumber - 1))
                    .limit(pageSize);
        }

        // return result list
        return productStream.collect(Collectors.toList());
    }

    @Override
    public Product getProductDetail(String userId, long productId) {
        Product product = productPanacheRepository.findById(productId);
        // validate existence and ownership
        if(product == null || !product.getOwnerId().equals(userId))
            throw new NotFoundException();
        return product;
    }

    @Override
    @Transactional
    public Product createProduct(String userId, Product product) {
        try {
            // validate values
            if(product.getPrice() < 0)
                throw new BadRequestException();

            // validate store existence and ownership
            Store store = storePanacheRepository.findById(product.getStore().getId());
            if(store == null || !store.getOwnerId().equals(userId))
                throw new BadRequestException();
            product.setStore(store);
            product.setOwnerId(userId);

            // validate tags existence and ownership
            validateAndUpdateProductTags(userId, product);

            // persist data
            productPanacheRepository.persist(product);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return product;
    }

    @Override
    @Transactional
    public Product updateProduct(String userId, Product product) {
        try {
            // validate values
            if(product.getPrice() < 0)
                throw new BadRequestException();

            // validate ownership
            Product foundProduct = productPanacheRepository.findById(product.getId());
            if(foundProduct == null || !foundProduct.getOwnerId().equals(userId))
                throw new BadRequestException();
            product.setOwnerId(userId);
            product.setStore(foundProduct.getStore());

            // validate tags existence and ownership
            validateAndUpdateProductTags(userId, product);

            // persist data
            productPanacheRepository.getEntityManager().merge(product);
        } catch (Exception e) {
            throw new BadRequestException();
        }
        return product;
    }

    @Override
    @Transactional
    public void deleteProduct(String userId, long productId) {
        productPanacheRepository.delete(getProductDetail(userId, productId));
    }

    private void validateAndUpdateProductTags(String userId, Product product) {
        Set<Tag> tags = new LinkedHashSet<>();
        product.getTags().forEach(tag -> {
            Tag foundTag = tagPanacheRepository.findById(tag.getId());
            if(foundTag == null || !foundTag.getOwnerId().equals(userId))
                throw new BadRequestException();
            tags.add(foundTag);
        });
        product.setTags(tags);
    }
}
