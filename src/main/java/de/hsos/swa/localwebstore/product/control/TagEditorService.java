package de.hsos.swa.localwebstore.product.control;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This Interface is for editing Tags
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface TagEditorService {
    Tag createTag(String userId, Tag tag);
    Tag updateTag(String userId, Tag tag);
    void deleteTag(String userId, long tagId);
}
