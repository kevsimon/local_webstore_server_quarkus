package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.control.ProductEditorService;
import de.hsos.swa.localwebstore.product.control.ProductReaderService;
import de.hsos.swa.localwebstore.product.entity.Product;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * This class accepts requests and gives results for the Product-Endpoint
 * @author Kevin Lucas Simon
 */
@Path("/api/v1/vendor/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "SecurityScheme", type = SecuritySchemeType.HTTP, scheme = "Bearer")
public class ProductResource {
    @Inject
    SecurityIdentity identity;

    @Inject
    ProductReaderService productReaderService;
    @Inject
    ProductEditorService productEditorService;

    @GET
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Collection<ProductForListDTO> getProductList(
            @QueryParam("filter[store-id]") long storeId, @QueryParam("filter[tag-id]") Set<Long> tagIds,
            @QueryParam("page[number]") int pageNumber, @QueryParam("page[size]") int pageSize) {
        // Convert filtered Product Collection to ProductDTO Collection and return result
        Collection<Product> products = productReaderService.getProductList(
                getAuthenticatedUserId(), pageNumber, pageSize, storeId, tagIds);
        Collection<ProductForListDTO> productDTOs = new ArrayList<>();
        products.forEach(product -> productDTOs.add(new ProductForListDTO(product)));
        return productDTOs;
    }

    @POST
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public ProductForDetailDTO createProduct(ProductForCreationDTO createdProduct) {
        return new ProductForDetailDTO(productEditorService.createProduct(getAuthenticatedUserId(), createdProduct.export()));
    }

    @GET
    @Path("/{product-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public ProductForDetailDTO getProductDetail(@PathParam("product-id") long productId) {
        return new ProductForDetailDTO(productReaderService.getProductDetail(getAuthenticatedUserId(), productId));
    }

    @PATCH
    @Path("/{product-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public ProductForDetailDTO updateProduct(@PathParam("product-id") long productId, ProductForUpdateDTO updatedProduct) {
        Product product = updatedProduct.export();
        product.setId(productId);
        return new ProductForDetailDTO(productEditorService.updateProduct(getAuthenticatedUserId(), product));
    }

    @DELETE
    @Path("/{product-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Response deleteProduct(@PathParam("product-id") long productId) {
        productEditorService.deleteProduct(getAuthenticatedUserId(), productId);
        return Response.ok().build();
    }

    private String getAuthenticatedUserId() {
        return ((DefaultJWTCallerPrincipal) identity.getPrincipal()).getSubject();
    }
}
