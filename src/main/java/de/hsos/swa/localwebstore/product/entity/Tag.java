package de.hsos.swa.localwebstore.product.entity;

import de.hsos.swa.localwebstore.store.entity.Store;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.enterprise.context.Dependent;
import javax.persistence.*;
import java.util.Collection;

/**
 * This Entity represents the Tag in the database
 * @author Kevin Lucas Simon
 */
@Entity
@Dependent
public class Tag {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String ownerId;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Store store;

    @ManyToMany(mappedBy = "tags")
    private Collection<Product> products;

    @PreRemove
    private void removeProductRelationBeforeDeletion() {
        products.forEach(product -> product.removeTag(this));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
