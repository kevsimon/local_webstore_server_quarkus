package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Product;

/**
 * This data transfer object is used for the list view of Products
 * @author Kevin Lucas Simon
 */
public class ProductForListDTO {
    private long id;
    private String name;
    private float price;

    public ProductForListDTO() {}

    public ProductForListDTO(Product product) {
        this.setId(product.getId());
        this.setName(product.getName());
        this.setPrice(product.getPrice());
    }

    public Product export() {
        Product product = new Product();

        product.setId(this.getId());
        product.setName(this.getName());
        product.setPrice(this.getPrice());

        return product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
