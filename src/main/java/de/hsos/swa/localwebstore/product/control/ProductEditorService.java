package de.hsos.swa.localwebstore.product.control;

import de.hsos.swa.localwebstore.product.entity.Product;

/**
 * This Interface is for editing Products
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface ProductEditorService {
    Product createProduct(String userId, Product product);
    Product updateProduct(String userId, Product product);
    void deleteProduct(String userId, long productId);
}
