package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This data transfer object is used for the update of Tags
 * @author Kevin Lucas Simon
 */
public class TagForUpdateDTO {
    private String name;

    public TagForUpdateDTO() {}

    public TagForUpdateDTO(Tag tag) {
        this.setName(tag.getName());
    }

    public Tag export() {
        Tag tag = new Tag();

        tag.setName(this.getName());

        return tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
