package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.store.entity.Store;

/**
 * This data transfer object is used for an reduced representation of the Store Entity for other DTOs
 * @author Kevin Lucas Simon
 */
public class StoreReducedDTO {
    private long id;

    public StoreReducedDTO() {}

    public StoreReducedDTO(Store store) {
        this.setId(store.getId());
    }

    public Store export() {
        Store store = new Store();

        store.setId(this.getId());

        return store;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
