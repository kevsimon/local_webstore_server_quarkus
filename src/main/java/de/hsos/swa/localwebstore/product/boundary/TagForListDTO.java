package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This data transfer object is used for the list view of Tags
 * @author Kevin Lucas Simon
 */
public class TagForListDTO {
    private long id;
    private String name;

    public TagForListDTO() {}

    public TagForListDTO(Tag tag) {
        this.setId(tag.getId());
        this.setName(tag.getName());
    }

    public Tag export() {
        Tag tag = new Tag();

        tag.setId(this.getId());
        tag.setName(this.getName());

        return tag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
