package de.hsos.swa.localwebstore.product.control;

import de.hsos.swa.localwebstore.product.entity.Product;

import java.util.Collection;

/**
 * This Interface is for reading Products
 * It can be used for injection
 * @author Kevin Lucas Simon
 */
public interface ProductReaderService {
    Collection<Product> getProductList(
            String userId, int pageNumber, int pageSize, long storeId, Collection<Long> tagIds);
    Product getProductDetail(String userId, long productId);
}
