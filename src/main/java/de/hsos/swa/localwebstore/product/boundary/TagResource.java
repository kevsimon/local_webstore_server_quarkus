package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.control.TagEditorService;
import de.hsos.swa.localwebstore.product.control.TagReaderService;
import de.hsos.swa.localwebstore.product.entity.Tag;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This class accepts requests and gives results for the Tag-Endpoint
 * @author Kevin Lucas Simon
 */
@Path("/api/v1/vendor/tags")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "SecurityScheme", type = SecuritySchemeType.HTTP, scheme = "Bearer")
public class TagResource {
    @Inject
    SecurityIdentity identity;

    @Inject
    TagReaderService tagReaderService;
    @Inject
    TagEditorService tagEditorService;

    @GET
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Collection<TagForListDTO> getTagList(
            @QueryParam("filter[store-id]") Long storeId, @QueryParam("page[number]") int pageNumber, @QueryParam("page[size]") int pageSize) {
        // Convert filtered Tag Collection to TagDTO Collection and return result
        Collection<Tag> tags = tagReaderService.getTagListFromStore(getAuthenticatedUserId(), pageNumber, pageSize, storeId);
        Collection<TagForListDTO> tagDTOs = new ArrayList<>();
        tags.forEach(tag -> tagDTOs.add(new TagForListDTO(tag)));
        return tagDTOs;
    }

    @POST
    @Path("/")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public TagForDetailDTO createTag(TagForCreationDTO createdTag) {
        return new TagForDetailDTO(tagEditorService.createTag(getAuthenticatedUserId(), createdTag.export()));
    }

    @GET
    @Path("/{tag-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public TagForDetailDTO getTagDetail(@PathParam("tag-id") long tagId) {
        return new TagForDetailDTO(tagReaderService.getTagDetail(getAuthenticatedUserId(), tagId));
    }

    @PATCH
    @Path("/{tag-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public TagForDetailDTO updateTag(@PathParam("tag-id") long tagId, TagForUpdateDTO updatedTag) {
        Tag tag = updatedTag.export();
        tag.setId(tagId);
        return new TagForDetailDTO(tagEditorService.updateTag(getAuthenticatedUserId(), tag));
    }

    @DELETE
    @Path("/{tag-id}")
    @RolesAllowed("vendor")
    @SecurityRequirement(name = "SecurityScheme")
    public Response delete(@PathParam("tag-id") long tagId) {
        tagEditorService.deleteTag(getAuthenticatedUserId(), tagId);
        return Response.ok().build();
    }

    private String getAuthenticatedUserId() {
        return ((DefaultJWTCallerPrincipal) identity.getPrincipal()).getSubject();
    }
}
