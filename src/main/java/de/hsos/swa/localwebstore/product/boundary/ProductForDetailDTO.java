package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Product;
import de.hsos.swa.localwebstore.product.entity.Tag;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This data transfer object is used for the detail view of Products
 * @author Kevin Lucas Simon
 */
public class ProductForDetailDTO {
    private long id;
    private String name;
    private String description;
    private float price;
    private boolean visible;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private StoreReducedDTO store;
    private Collection<TagReducedDTO> tags;

    public ProductForDetailDTO() {}

    public ProductForDetailDTO(Product product) {
        this.setId(product.getId());
        this.setName(product.getName());
        this.setDescription(product.getDescription());
        this.setPrice(product.getPrice());
        this.setVisible(product.isVisible());
        this.setCreatedAt(product.getCreatedAt());
        this.setUpdatedAt(product.getUpdatedAt());

        this.setStore(new StoreReducedDTO(product.getStore()));
        Collection<TagReducedDTO> tags = new ArrayList<>();
        product.getTags().forEach(tag -> tags.add(new TagReducedDTO(tag)));
        this.setTags(tags);
    }

    public Product export() {
        Product product = new Product();

        product.setId(this.getId());
        product.setName(this.getName());
        product.setDescription(this.getDescription());
        product.setPrice(this.getPrice());
        product.setVisible(this.isVisible());
        product.setCreatedAt(this.getCreatedAt());
        product.setUpdatedAt(this.getUpdatedAt());

        if(this.getStore() != null)
            product.setStore(this.getStore().export());
        Collection<Tag> tags = new ArrayList<>();
        this.getTags().forEach(tag -> tags.add(tag.export()));
        product.setTags(tags);

        return product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public StoreReducedDTO getStore() {
        return store;
    }

    public void setStore(StoreReducedDTO store) {
        this.store = store;
    }

    public Collection<TagReducedDTO> getTags() {
        return tags;
    }

    public void setTags(Collection<TagReducedDTO> tags) {
        this.tags = tags;
    }
}
