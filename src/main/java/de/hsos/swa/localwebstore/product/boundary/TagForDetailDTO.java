package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Tag;

/**
 * This data transfer object is used for the detail view of Tags
 * @author Kevin Lucas Simon
 */
public class TagForDetailDTO {
    private long id;
    private String name;
    private StoreReducedDTO store;

    public TagForDetailDTO() {}

    public TagForDetailDTO(Tag tag) {
        this.setId(tag.getId());
        this.setName(tag.getName());
        this.setStore(new StoreReducedDTO(tag.getStore()));
    }

    public Tag export() {
        Tag tag = new Tag();

        tag.setId(this.getId());
        tag.setName(this.getName());

        if(this.getStore() != null)
        tag.setStore(this.getStore().export());

        return tag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreReducedDTO getStore() {
        return store;
    }

    public void setStore(StoreReducedDTO store) {
        this.store = store;
    }
}
