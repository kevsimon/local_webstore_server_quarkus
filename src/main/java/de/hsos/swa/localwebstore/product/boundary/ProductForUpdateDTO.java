package de.hsos.swa.localwebstore.product.boundary;

import de.hsos.swa.localwebstore.product.entity.Product;
import de.hsos.swa.localwebstore.product.entity.Tag;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This data transfer object is used for the update of Products
 * @author Kevin Lucas Simon
 */
public class ProductForUpdateDTO {
    private String name;
    private String description;
    private float price;
    private boolean visible;
    private Collection<TagReducedDTO> tags;

    public ProductForUpdateDTO() {}

    public ProductForUpdateDTO(Product product) {
        this.setName(product.getName());
        this.setDescription(product.getDescription());
        this.setPrice(product.getPrice());
        this.setVisible(product.isVisible());

        Collection<TagReducedDTO> tags = new ArrayList<>();
        product.getTags().forEach(tag -> tags.add(new TagReducedDTO(tag)));
        this.setTags(tags);
    }

    public Product export() {
        Product product = new Product();

        product.setName(this.getName());
        product.setDescription(this.getDescription());
        product.setPrice(this.getPrice());
        product.setVisible(this.isVisible());

        Collection<Tag> tags = new ArrayList<>();
        this.getTags().forEach(tag -> tags.add(tag.export()));
        product.setTags(tags);

        return product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Collection<TagReducedDTO> getTags() {
        return tags;
    }

    public void setTags(Collection<TagReducedDTO> tags) {
        this.tags = tags;
    }
}
